const isNumber = (val) => {
   const toNumber = Number(val)
   return (isNaN(toNumber)) 
           ? false
           : true
}

const validDay = (num) => {
   if (num < 31 && num > 0) {
       if(document.getElementById('day').classList.contains('error')){
           document.getElementById('day').classList.remove('error')   
       }
       return true
   }
   document.getElementById('day').classList.add('error')
   return false
}

const validMonth = (num) => {
   if (num < 12 && num > 0) {
       if(document.getElementById('month').classList.contains('error')){
           document.getElementById('month').classList.remove('error')   
       }
       return true
   }
   document.getElementById('month').classList.add('error')
   return false
}

const validYear = (num) => {
   if (num < 2020 && num > 1900) {
       if(document.getElementById('year').classList.contains('error')){
           document.getElementById('year').classList.remove('error')   
       }
       return true
   }
   document.getElementById('year').classList.add('error')
   return false
}

const dateIsValid = (arrNums) => {
   if(arrNums.includes(NaN)){
       return false
   }else{
       const vDay = validDay(arrNums[0])
       const vMonth = validMonth(arrNums[1])
       const vYear = validYear(arrNums[2])
       return vDay && vMonth && vYear
   }
}

const isDate = () => {
   const day = document.getElementById('day').value 
   const month = document.getElementById('month').value
   const year = document.getElementById('year').value

   const date = [day, month, year]
   
   const dateInNumber = date.map( value => {
       return (isNumber(value))
               ? Number(value)
               : NaN
   })

   const valid = dateIsValid(dateInNumber)

   let dateFormat = ''

   if(valid === false){
       console.log("Aqui la cagaste bien feo")
       return null
   }else{
       console.log("Todo bien crack en la fecha")
       dateInNumber.map(num => {
           if(dateFormat[0] === num){
               dateFormat = `${num}/`    
           }else if(dateFormat[2] === num){
               dateFormat = `${dateFormat}${num}/`
           }else{
               dateFormat = `${dateFormat}${num}`
           }
       })
   }

   return new Date(dateFormat)
}

const isEmail = () => {
   const correo = document.getElementById("email").value;
   const expRegCorreo=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

   if (expRegCorreo.test(correo)){
       if(document.getElementById('email').classList.contains('error')){
           document.getElementById('email').classList.remove('error')   
       }
       return true
   }
   else {
       document.getElementById('email').classList.add('error')
       return false
   }
}

const samePass = () => {
   const pass1 = document.getElementById('password').value 
   const pass2 = document.getElementById('password2').value

   if (pass1 === pass2) {
       if(document.getElementById('password2').classList.contains('error')){
           document.getElementById('password2').classList.remove('error')   
       }
       return true
   }
   document.getElementById('password2').classList.add('error')
   return false
}

const validar = () => {
   const dateValidated = isDate()
   // samePass es bool
   const userValidated = isUser()
   const samePassValidated = samePass()
   const emailValidated = isEmail()

   if (dateValidated !== null && samePassValidated && userValidated && emailValidated) {
       alert("Está bien")
       return
   }
   alert("Está re mal wacho")
}

const isUser = () => {
const nombre = document.getElementById("name").value;
const expRegNombre=/^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;

if (expRegNombre.test(nombre)) {
   if (document.getElementById('name').classList.contains('error')) {
       document.getElementById('name').classList.remove('error')
       return true
   }
}
else {
   document.getElementById('name').classList.add('error')
   return false
}    
}