exports.edad = (fechaN) => {
    const fechaD = new Date()
    const faltaTime = fechaD.getTime() - fechaN.getTime()
    const faltaYears = faltaTime / (1000 * 3600 * 24 * 365)
    return faltaYears
}